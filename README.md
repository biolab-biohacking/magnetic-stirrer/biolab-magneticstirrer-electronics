# Magnetic Stirrer

Schematic for a DIY Magnetic Stirrer

This is a 2 part PCB:

One for the control

    Power Supply Input

    Stirrer Velocity

    Stirrer Switch

    LED

    Switch for each heating resistor

The other PCB

    is the heating component, its only a derivation of the main board.

    Wires must be soldered to bridge the two PCB

## Preview

### Front

![](photos/magnetic_stirrerV0.1_front.png)

![](photos/magnetic_stirrerV0.1_front_rendered.png)

### Back

![](photos/magnetic_stirrerV0.1_back.png)
